package bd.com.mcc.exmapleretrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Belal on 10/2/2017.
 */

public interface Api {

    String BASE_URL = "https://nationalappsbangladesh.com/mobsvc/ContentFile.php/";

    @GET("marvel")
    Call<List<Hero>> getHeroes();
}