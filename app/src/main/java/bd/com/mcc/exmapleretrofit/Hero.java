package bd.com.mcc.exmapleretrofit;

import com.google.gson.annotations.SerializedName;

public class Hero {

    @SerializedName("Id")
    private String Id;
   @SerializedName("IMG")
    private String IMG;

    
    private String team;
    private String firstappearance;
    private String createdby;
    private String publisher;
    private String imageurl;
    private String bio;


    public Hero(String Id, String IMG, String team, String firstappearance, String createdby, String publisher, String imageurl, String bio) {
        this.Id = Id;
        this.IMG = IMG;
        this.team = team;
        this.firstappearance = firstappearance;
        this.createdby = createdby;
        this.publisher = publisher;
        this.imageurl = imageurl;
        this.bio = bio;
    }

    public String getId() {
        return Id;
    }

    public String getIMG() {
        return IMG;
    }

    public String getTeam() {
        return team;
    }

    public String getFirstappearance() {
        return firstappearance;
    }

    public String getCreatedby() {
        return createdby;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getImageurl() {
        return imageurl;
    }

    public String getBio() {
        return bio;
    }
}